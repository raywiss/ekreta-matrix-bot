#!/usr/bin/python3.7

from typing import Text
import requests
import time
import datetime
import sys
import os
import asyncio
import kreta_v2
from nio import AsyncClient
from bs4 import BeautifulSoup
from requests.api import head
import pickle
import json
import copy

def convert_time(dt):
    txt=datetime.datetime.fromisoformat(
                dt.replace('Z','+00:00')).astimezone().strftime('%Y.%m.%d. %H:%M')
    return txt
def convert_date(dt):
    txt=datetime.datetime.fromisoformat(
                dt.replace('Z','+00:00')).astimezone().strftime('%Y.%m.%d.')
    return txt

def get_str(dictionary,*args,placeholder='-'):
    d=dictionary
    for kw in args:
        if not d[kw]:
            return placeholder
        d=d[kw]
    return d

class Configini(object):
    def __init__(self) -> None:
        from configparser import ConfigParser
        super().__init__()
        self._config_parser=ConfigParser()
        for inifile in ['./kreta.ini','/etc/kreta/kreta.ini','/usr/local/etc/kreta/kreta.ini']:
            if os.path.isfile(inifile):
                self._config_parser.read(inifile)
                break
        self._config_section=sys.argv[1]
        self._config=dict()
        self._config['user']=self._config_parser.get(self._config_section,'user')
        self._config['pass']=self._config_parser.get(self._config_section,'pass')
        self._config['school']=self._config_parser.get(self._config_section,'school')
        self._config['picklefile']=self._config_parser.get(self._config_section,'picklefile')

        self._config['matrix_user']=self._config_parser.get(self._config_section,'matrix_user')
        self._config['matrix_password']=self._config_parser.get(self._config_section,'matrix_password')
        self._config['matrix_hs']=self._config_parser.get(self._config_section,'matrix_hs')
        self._config['matrix_room']=self._config_parser.get(self._config_section,'matrix_room')
    
    def get(self,key):
        return self._config[key]

class Matrix(object):
    def __init__(self,config):
        self._config=config
        self._server = self._config.get('matrix_hs')
        self._user = self._config.get('matrix_user')
        self._password = self._config.get('matrix_password')
        self._room=self._config.get('matrix_room')
        self._client = AsyncClient(self._server, self._user)

    async def send_message(self,messages):
        await self._client.login(self._password)
        await self._client.join(self._room)
        _pickle=ProcessHashes(config.get('picklefile'))
        for data in messages:
            print('Sending: ['+data['hash']+']')
            message=data['text']
            content = {
                    "msgtype": "m.text",
                    "format" : "org.matrix.custom.html",
                    "body": BeautifulSoup(message,"html.parser").text,
                    "formatted_body": message
                }
            # print(content)
            await self._client.room_send(
                room_id=self._room,
                message_type="m.room.message",
                content=content
            )
            _pickle.put_hash(data['hash'])
        _pickle.chop_and_store()
        await self._client.close()

class ProcessHashes(object):
    def __init__(self,hash_file):
        self._hash_file = hash_file
        try:
            with open(self._hash_file, 'rb') as f:
                # The protocol version used is detected automatically, so we do not
                # have to specify it.
                self._hash = pickle.load(f)
        except:
            self._hash = []
            with open(self._hash_file, 'wb') as f:
                # Pickle the 'data' dictionary using the highest protocol available.
                pickle.dump(self._hash, f)

    def is_hash_exists(self,hash_value):
        if hash_value in self._hash:
            return True
        else:
            return False

    def put_hash(self,hash_value):
        self._hash.insert(0,hash_value)

    def chop_and_store(self):
        self.chop_hash()
        self.pickle_hash()

    def chop_hash(self):
        self._hash = self._hash[0:60]

    def pickle_hash(self):
        with open(self._hash_file, 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(self._hash, f)

class KretaClass(object):
    def __init__(self,config,student) -> None:
        super().__init__()
        self._user=config.get('user')
        self._pass=config.get('pass')
        self._school=config.get('school')
        self._kreta=kreta_v2.User(self._user,self._pass,self._school)
        self._student=student

    def set_evaluations(self):
        evaluations=self._kreta.getEvaluations()
        for evaluation in evaluations:
            evaluation['RogzitesDatuma']=convert_date(evaluation['RogzitesDatuma'])            
        self._student.set_evaulations(evaluations)

    def set_userinfo(self):
        self._student.set_userinfo(json.loads(self._kreta.getInfo()))

    def set_announcements(self):
        announcements=json.loads(self._kreta.getAnnounced())
        for announcement in announcements:
            announcement['Datum']=convert_date(announcement['Datum'])
            announcement['BejelentesDatuma']=convert_date(announcement['BejelentesDatuma'])
        self._student.set_announcements(announcements)

    def set_absences(self):
        absences=json.loads(self._kreta.getAbsences())
        for absence in absences:
            absence['Ora']['KezdoDatum']=convert_time(absence['Ora']['KezdoDatum'])
            absence['Ora']['VegDatum']=convert_time(absence['Ora']['VegDatum'])
        self._student.set_absences(absences)

    def set_incoming_messages(self):
        incoming_messages=self._kreta.getMessages('beerkezett')
        self._student.set_incoming_messages(incoming_messages)

    def set_timetable(self,from_date='',to_date=''):
        timetable=self._kreta.getTimetable(from_date,to_date)
        self._student.set_timetable(timetable)

class Student(object):
    def __init__(self,config) -> None:
        super().__init__()
        self._config=config
        self._pickle=ProcessHashes(config.get('picklefile'))
        self._evaluations=None
        self._not_seen_evaluations=None
        self._announcements=None
        self._not_seen_announcements=None
        self._absences=None
        self._not_seen_absences=None
        self._incoming_messages=None
        self._not_seen_incoming_messages=None
        self._timetable=None
        self._not_seen_timetable=None
    
    def set_evaulations(self,evaluations):
        self._evaluations=copy.copy(evaluations)
    
    def set_userinfo(self,userinfo):
        self._userinfo=copy.copy(userinfo)

    def set_announcements(self,announcements):
        self._announcements=copy.copy(announcements)

    def set_absences(self,absences):
        self._absences=copy.copy(absences)

    def set_incoming_messages(self,incoming_messages):
        self._incoming_messages=copy.copy(incoming_messages)

    def set_timetable(self,timetable):
        self._timetable=copy.copy(timetable)

    def get_name(self):
        return self._userinfo['Nev']

    def get_not_seen_absences(self):
        if self._not_seen_absences:
            return self._not_seen_absences
        self._not_seen_absences=[]
        for absence in self._absences:
            if not self._pickle.is_hash_exists(absence['Uid']):
                self._not_seen_absences.append(absence)
        return self._not_seen_absences

    def get_not_seen_evaluations(self):
        if self._not_seen_evaluations:
            return self._not_seen_evaluations
        self._not_seen_evaluations=[]
        for evaluation in self._evaluations:
            if not self._pickle.is_hash_exists(evaluation['Uid']):
                self._not_seen_evaluations.append(evaluation)
        return self._not_seen_evaluations
    
    def get_matrix_text_from_absence(self,absence):
        txt='''<h1>Hiányzás</h1>
<h2>Tanuló: %s</h2>

<strong>Tantárgy:</strong> %s<br />
<strong>Dátum:</strong> %s - %s<br />
<hr />
''' % ( self.get_name(),
        get_str(absence,'Tantargy','Nev'),
        get_str(absence,'Ora','KezdoDatum'),
        get_str(absence,'Ora','VegDatum'))
        return txt
    
    def get_all_not_seen_absences_matrix_text(self):
        txt=list()
        for absence in self.get_not_seen_absences():
            txt.append({'hash': absence['Uid'],
                        'text': self.get_matrix_text_from_absence(absence)})
        return txt

    def get_matrix_text_from_evaluation(self,evaluation):
        txt='''<h1>Értékelés</h1>
<h2>Tanuló: %s</h2>

<strong>Tantárgy:</strong> %s<br />
<strong>Téma:</strong> %s<br />
<strong>Osztályzat:</strong> %s<br />
<strong>Súly:</strong> %s%%<br />
<strong>Mód:</strong> %s<br />
<strong>Dátum:</strong> %s<br />
<hr />
''' % ( self.get_name(),
        get_str(evaluation,'Tantargy','Nev'),
        get_str(evaluation,'Tema'),
        get_str(evaluation,'SzovegesErtek'),
        get_str(evaluation,'SulySzazalekErteke'),
        get_str(evaluation,'Mod','Leiras'),
        get_str(evaluation,'RogzitesDatuma'))
        return txt
    
    def get_all_not_seen_evaluation_matrix_text(self):
        txt=list()
        for evaluation in self.get_not_seen_evaluations():
            txt.append({'hash': evaluation['Uid'],
                        'text': self.get_matrix_text_from_evaluation(evaluation)})
        return txt

    def get_not_seen_announcements(self):
        if self._not_seen_announcements:
            return self._not_seen_announcements
        self._not_seen_announcements=[]
        for announcement in self._announcements:
            if not self._pickle.is_hash_exists(announcement['Uid']):
                self._not_seen_announcements.append(announcement)
        return self._not_seen_announcements
    
    def get_matrix_text_from_announcement(self,announcement):
        txt='''<h1>Bejelentés</h1>
<h2>%s</h2>
<h3>Tanuló: %s</h3>

<strong>Tantárgy:</strong> %s<br />
<strong>Téma:</strong> %s<br />
<strong>Mód:</strong> %s<br />
<strong>Dátum:</strong> %s<br />
<hr />
''' % ( get_str(announcement,'Modja','Leiras'),
        self.get_name(),
        get_str(announcement,'Tantargy','Nev'),
        get_str(announcement,'Temaja'),
        get_str(announcement,'Modja','Leiras'),
        get_str(announcement,'Datum'))
        return txt

    def get_all_not_seen_announcement_matrix_text(self):
        txt=list()
        for announcement in self.get_not_seen_announcements():
            txt.append({'hash': announcement['Uid'],
                        'text': self.get_matrix_text_from_announcement(announcement)})
        return txt

    def get_all_matrix_text(self):
        txt_list=list()
        txt_list.extend(self.get_all_not_seen_evaluation_matrix_text())
        txt_list.extend(self.get_all_not_seen_announcement_matrix_text())
        txt_list.extend(self.get_all_not_seen_absences_matrix_text())
        return txt_list

config=Configini()
student=Student(config)
kreta_class=KretaClass(config,student)
kreta_class.set_userinfo()
kreta_class.set_evaluations()
kreta_class.set_announcements()
kreta_class.set_absences()
kreta_class.set_incoming_messages()
kreta_class.set_timetable(from_date='2021-09-28',to_date='2021-09-29')

matrix=Matrix(config)
asyncio.get_event_loop().run_until_complete(matrix.send_message(student.get_all_matrix_text()))
